const fs = require('fs')
var result=[]

function setObject(data){
    //Return the promise
    return new Promise((resolve,reject)=>{
        //Declare an variable object
        var obj = {}
        //If the id is null do not add that data.
        if(data[0]===''){
            //console.log(data)
            resolve()
        }else{
            //Set the various attributes of the data in the object and then push that object in the list
            obj["_id"]= data[0]
            obj["name"] = data[1]
            obj["price"]= parseInt(data[2])
            obj["seller"] = data[3]
            obj["quantity"] = parseInt(data[4])
            obj["department"] = data[5]
            //Push the object in the list
            result.push(obj)
            //console.log(obj)
            //Resolve the promise
            resolve()
            }
            
    })
    
}

function tsvToJSON(data){
    //Return a promise
    return new Promise((resolve,reject)=>{
        //data contains the string from the file. seperate it using "\n" so we get each line containing information about the product
        var lines = data.split("\n")
        //create a list of promise
        var promises = []
        //Iterate through each line
        for (var i =0;i<lines.length;i++){
            //Each line contains information about product and each information is seperated using "\t". So we split it
            var current = lines[i].split("\t")
            //Get the promise to insert the product into the list
            var promise = setObject(current)
            //Push that promise into the list of promises
            promises.push(promise)
        }
        //Wait for all the promise to be completed
        Promise.all(promises).then(()=>{
        //console.log(result)
            //Send the result list which contains each products.
            resolve(result)
        })
    })
    
}

function insertFile(db){
    //Return the promise
    return new Promise((resolve,reject)=>{
        //Open the file
        ///usr/src/app/
        fs.readFile('/usr/src/app/'+'sample_product_data.tsv','utf-8',(err,data)=>{
            if(err){
                //Error is reading file
                reject(err)
            }else{
               //Get the products 
               tsvToJSON(data).then((products)=>{
                    var promises = []
                    //Iterate through each object as we need to set the id.  
                    for (product of products){
                        
                        //Convert the camelCase to SnakeCase
                        
                        //Promises of pushing the document in the collection
                        var promise = new Promise((resolve,reject)=>{
                            db.collection("products").insert(product,(err,data)=>{
                                if(err){
                                    reject(err)
                                }else{
                                    resolve()
                                }
                            })
                        })
                        //Push the promise in the array.
                        promises.push(promise)
                    
                    }
                    //Wait for all the promises to be completed
                    Promise.all(promises).then(()=>{
                        //Get the count of the documents pushed in the collection
                        countDocuments(db).then((data)=>{
                            //Return that number
                            resolve(data)
                        }).catch((err)=>{
                            //Catch any Error
                            reject(err)
                        })
                    }).catch((err)=>{
                        //Catch error in insering documents 
                        reject(err)
                    })
               })
               
            }
        })
    })
    
}


function checkAndInsertFile(db,fileName){
    //return the promise
    return new Promise((resolve,reject)=>{
        //check for the files collection in the databse
        db.collection('files').find({},(err,resp)=>{
            if(err){
                //Error
                reject(err)
                
            }else{
                //Count the response object if 0, insert the file, if greater than 0 check if the file exists, if not insert the file
                resp.count().then((data)=>{
                    //check the data if 0
                    if (data == 0){
                        //insert the metadata about the file and then the file itself.
                        db.collection("files").insert({'file':fileName},(err,resp)=>{
                            //Check for errors
                            if(err){
                                reject(err)
                            }
                            //Insert the file get the promise
                            var promise = insertFile(db,fileName)
                            //Wait till the promise gets resolved
                            Promise.all([promise]).then((data)=>{
                                resolve(data)
                            }).catch((err)=>{
                                //Delete the files collection
                                db.collection("files").deleteMany({},(err,r)=>{

                                })
                                reject(err)
                            })
                            //resolve("Successfully added file in files")
                            
                        })
                    }else{
                        //There are files in the database. 
                        resp.toArray((err,data)=>{
                            if(err){
                                reject(err)
                            }
                            resolve(0)
                        })
                    }
                })
                
            }
        })
    })
    
}

function countDocuments(db){
    //Return the promise
    return new Promise((resolve,reject)=>{
        //Get the number of documents in the collections
        db.collection("products").count((err,data)=>{
            if(err){
                //Return any error
                reject(err)
            }
            //Return the number of the documents
            resolve(data)
        })
    })
}


module.exports = {
    insertFile : checkAndInsertFile
}