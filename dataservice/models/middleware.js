var mongo = require('mongodb').MongoClient

function getConnection(req,res,next){
   
    mongo.connect("mongodb://mongo:27017/stackline",(err,database)=>{
        //console.log(database)
        req.database = database
        next()
    })
    
}

module.exports={
    getConnection: getConnection
}