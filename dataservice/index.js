var app = require('express')()
var mongo = require('mongodb').MongoClient
var middleware = require('./models/middleware')
var insert = require('./models/inserting')


mongo.connect("mongodb://mongo:27017/stackline",(err,database)=>{
    //Check for error
    if(err){
        throw(err)
    }
    db = database
    //Listen at the port
    app.listen(5000,()=>{
        console.log("Listening on port 5000")
    })
})

app.get('/import',middleware.getConnection,(req,res)=>{
   
    insert.insertFile(req.database,"sample_product_data.tsv").then((data)=>{
        console.log(data)
        var response = JSON.parse('{"count":' + data +'}')
        res.send(response)

    }).catch((err)=>{
        console.log(err)
        res.sendStatus(503)
    })
    
})


