## Stackline-project

The project desciption : To build an API using the given S3 file.

## Overall Solution

Use the .tsv file given, format the file's data, import that data in MongoDB database and develop various API endpoint based on that database.

There are two main modules in these project :
    1. DataService Module.
    2. Application Module. 

1. DataService Module.:
    Contains a single endpoint which will import the given file into the database and give the count of the data that are inserted in the database.


2. Application Module:
    Contains the neccessary endpoints through which you can interact with the data. 

## Data Service Module.

By calling :5000/import endpoint, the sample_product_data.tsv file will be imported into MongoDB database. Here no authentication is required to import the file. 

I have created the following object in JSON using the .tsv file and pushed the list of the objects in the database

{
    "_id": "B00WTAUJS2",
    "name": "Tamron SP 70-200mm f/2.8 Di VC USD Telephoto Zoom Lens for CANON Digital SLR Cameras w/ Essential Travel bundle",
    "price": 36333,
    "seller": "Goja",
    "quantity": 175,
    "department": "Camera Lenses"
}

Endpoint: ":5000/import"
The response will be 

JSON:
{
    "count":50000
}
if the file is not present in the database and 

JSON:
{
    "count:0
}
 if the file is already present in the database.


## Application Module.

All the endpoints of the application module can be accessed using port :3000. Here the authentication is required in every API call so it becomes neccessary to first obtain the JWT by 

Endpoint: ":3000/login" (POST)
Headers: "application/json"
Body: {
    "username":"user",
    "password":"pass"
    }

Response:
JSON:
{
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoidXNlciIsInBhc3MiOiJwYXNzIn0sImlhdCI6MTUyNjE1OTcyOX0.EXkylGr_CC2K85eE6JRCyoz8FeFrJevS2_4GyWDr3lk"
}    

Then we need to use this token in every API call.

Endpoint: ":3000/api/product/"
Header: token

Response:
JSON:
{
    "_id": "B00WTAUJS2",
    "name": "Tamron SP 70-200mm f/2.8 Di VC USD Telephoto Zoom Lens for CANON Digital SLR Cameras w/ Essential Travel bundle",
    "price": 36333,
    "seller": "Goja",
    "quantity": 175,
    "department": "Camera Lenses"
}


Hence the list of all the endpoints and there respective functions:

/api/login :- Logs the users and issues a JWT.

/api/product/:id :- Give products based on ID

/api/departments :- Give the list of the departments/category present in the dataset

/api/departments/:id :- Give all the prodcuts related to particular department / category

/api/products/departments :- Give the number of products present in each department/category

/api/products/?price=10000 :- Give all the products which has price greater than 10,000

/api/products/seller/:id :- Give all the products sold by particular seller.


## Setting up the enviornment:

We can use the docker-compose file that I have developed and access all the API endpoints.

## Testing the API's

In the test branch I have set up unit testing frameworks using "Chai", "Mocha", "Supertest". I have in total 8 tests but by no means are the complete test suites.


