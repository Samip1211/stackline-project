var mongo = require('mongodb').MongoClient

function getToken(req,res,next){

    const bearerHeader = req.headers['authorization']
    
    if(typeof bearerHeader !== 'undefined' ){
        req.token = bearerHeader.split(' ')[1]
    }else{
        res.sendStatus(403)
    }
    next()
}
function getConnection(req,res,next){
   
    mongo.connect("mongodb://mongo:27017/stackline",(err,database)=>{
        //console.log(database)
        req.database = database
        next()
    })
    
}
module.exports={
    getToken: getToken,
    getConnection: getConnection
}