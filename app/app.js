const app = require('express')()
const jwt = require('jsonwebtoken')
const mongo = require('mongodb').MongoClient
var middleware = require('./middleware/middleware')
const bodyParser = require("body-parser");
var search = require('./models/searching')

mongo.connect("mongodb://mongo:27017/stackline",(err,database)=>{
    //Check for error
    if(err){
        throw(err)
    }
    db = database
    //Listen at the port
    app.listen(3000,()=>{
        console.log("Listening on port 3000")
    })
})
app.use(bodyParser.json());
app.post('/login',(req,res)=>{
    
    if(req.headers["content-type"]==="application/json"){
        if(typeof req.body.username !== 'undefined' && typeof req.body.password !== 'undefined'){
            //Create temp User. Need to create user's entry in Database which will be used in future
            const user = {
                username: req.body.username,
                pass: req.body.password
            }
           
            jwt.sign({user:user},'secret',(err,token)=>{
                res.json({token})
            })
    
        }else{
            res.sendStatus(400)
        }
    }else{
        res.sendStatus(403)
    }
    
})
//An API endpoint to get the individual product based on id
app.get('/api/product/:id',middleware.getToken,middleware.getConnection,(req,res)=>{
    
    jwt.verify(req.token,'secret',(err,data)=>{
        if(err){
            res.sendStatus(403)
        }else{
            var id = req.params.id
            //console.log(req.database)
            //The function to get the product based on id
            search.searchProduct(req.database,id).then((data)=>{
                //Send the data
                if(data==null){
                    res.send("No data found")
                }
                res.send(data)

            }).catch((err)=>{
                console.log(err)
                res.sendStatus(503)
            })
        }
    })
    
})
//An api endpoint which will get the departments present
app.get('/api/departments',middleware.getToken,middleware.getConnection,(req,res)=>{
    
    jwt.verify(req.token,'secret',(err,data)=>{
        if(err){
            res.sendStatus(403)
        }else{
            search.searchDepartments(req.database).then((data)=>{
                if(data==null){
                    res.send("No Data Found")
                }
                res.send(data)
            }).catch((err)=>{
                console.log(err)
                res.sendStatus(503)
            })
        }
    })
})
//An api endpoint which will get the departments present
app.get('/api/departments/:id',middleware.getToken,middleware.getConnection,(req,res)=>{
    
    jwt.verify(req.token,'secret',(err,data)=>{
        if(err){
            res.sendStatus(403)
        }else{
            search.searchProductsByDepartment(req.database,req.params.id).then((data)=>{
                if(data==null){
                    res.send("No Data Found")
                }
                res.send(data)
            }).catch((err)=>{
                console.log(err)
                res.sendStatus(503)
            })
        }
    })
})
//An API endpoint which will give the count of the products by department
app.get('/api/products/departments',middleware.getToken,middleware.getConnection,(req,res)=>{
    jwt.verify(req.token,'secret',(err,data)=>{
        if(err){
            res.sendStatus(403)
        }else{
            search.searchProductsInDepartment(req.database,req.params.id).then((data)=>{
                if(data==null){
                    res.send("No Data Found")
                }
                res.send(data)
            }).catch((err)=>{
                console.log(err)
                res.sendStatus(503)
            })
        }
    })
})
//Function to get the products above a certain price amount
app.get('/api/products',middleware.getToken,middleware.getConnection,(req,res)=>{
    jwt.verify(req.token,'secret',(err,data)=>{
        if(err){
            res.sendStatus(403)
        }else{
            if( req.query.price == ''){
                search.searchProductsByPrice(req.database,req.query.price).then((data)=>{
                    if(data==null){
                        res.send("No Data Found")
                    }
                    res.send(data)
                }).catch((err)=>{
                    console.log(err)
                    res.sendStatus(503)
                })
            }else{
                res.sendStatus(404)
            }
            
        }
    })
})
//Function to get the products sold by particular seller
app.get('/api/products/seller/:id',middleware.getToken,middleware.getConnection,(req,res)=>{
    jwt.verify(req.token,'secret',(err,data)=>{
        if(err){
            res.sendStatus(403)
        }else{
            
            search.searchProductsBySeller(req.database,req.params.id).then((data)=>{
                if(data==null){
                    res.send("No Data Found")
                }
                res.send(data)
            }).catch((err)=>{
                console.log(err)
                res.sendStatus(503)
                })
            
            
        }
    })
})

