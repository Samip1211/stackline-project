//Function to format the data 
function formatProductDeptCount(data){
    if(data==null){
        return data
    }else{
        var response = '['
        //Since the data is in array format.
        for(value of data){
            //console.log(value["_id"] + value["count"])
            
            response = response + '{\"'+ value["_id"].toString() + "\":"+ parseInt(value["count"])+"}," 
        }
        //End the response array
        response = response + "]"
        //Remove the trailing ','
        response = response.replace(",]","]")
       
        return JSON.parse(response)
    }
}
//Function to search the product by particular id
function searchProduct(db,id){
    //Return the promise
    return new Promise((resolve,reject)=>{
        //Find the product with the given ID
        db.collection("products").find({"_id":id}).toArray((err,resp)=>{
            //res.sendStatus(200)
            if(err){
                reject(err)
            }
            //Send the product's info
            resolve(resp[0])
        })
    })
}
//Search the unique departments or/ categories 
function searchDepartments(db){
    //Return the promise
    return new Promise((resolve,reject)=>{
        //Database call
        db.collection("products").aggregate([
            {$group:{_id: "1",department:{$addToSet:"$department"}}}
        ],(err,data)=>{
            if(err){
                reject(err)
            }
            //console.log(data)
            resolve(data[0])
        })
    
    })
}
//Search the product by particular departments or/ categories
function searchProductsByDepartment(db,id){
    //Return the promise
    return new Promise((resolve,reject)=>{
        //Database call
        db.collection("products").find({"department":id}).toArray((err,data)=>{
            if(err){
                reject(err)
            }
            console.log(data)
            resolve(data)
        })
    })
}
//Function to give the number of products in each department
function searchProductsInDepartment(db,id){
    //Return the promise
    return new Promise((resolve,reject)=>{
        //Database call
        db.collection("products").aggregate([
            {$group:{ _id:"$department",count:{$sum:1}}}
        ],(err,data)=>{
            //console.log(data)
            
            resolve(formatProductDeptCount(data))
        })
    })
}
function searchProductsByPrice(db,id){
    //Return the promise
    return new Promise((resolve,reject)=>{
        //Find the product with the given ID
        db.collection("products").find({"price":{$gte:parseInt(id)}}).toArray((err,resp)=>{
            //res.sendStatus(200)
            if(err){
                reject(err)
            }
            //Send the product's info
            console.log(resp)
            resolve(resp)
        })
    })
}

module.exports={
    searchProduct: searchProduct,
    searchDepartments: searchDepartments,
    searchProductsByDepartment : searchProductsByDepartment,
    searchProductsInDepartment: searchProductsInDepartment,
    searchProductsByPrice: searchProductsByPrice

}